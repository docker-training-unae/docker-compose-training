<?php
$host = 'postgres-curso';
$db = 'docker';
$user = 'postgres';
$pass = '1234';

$dsn = "pgsql:host=$host;dbname=$db;user=$user;password=$pass";
$pdo = new PDO($dsn);

$stmt = $pdo->query('SELECT * FROM public.nombres');
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo "<h2>Tabla:</h2>";
echo "<table border='1'>";
echo "<tr><th>ID</th><th>Columna1</th></tr>";
foreach ($rows as $row) {
    echo "<tr><td>{$row['id']}</td><td>{$row['name']}</td></tr>";
}
echo "</table>";
?>