<?php
$host = 'db'; // Puede ser 'localhost' si PostgreSQL está en la misma máquina.
$port = '5432';
$dbname = 'mydatabase';
$user = 'myuser';
$password = 'mypasword';

// Intentar conectar a la base de datos
try {
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error al conectar a la base de datos: " . $e->getMessage());
}

// Ejemplo de consulta
try {
    // Preparar y ejecutar la consulta
    $query = "SELECT * FROM example";
    $statement = $pdo->prepare($query);
    $statement->execute();

    // Obtener resultados
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    // Imprimir resultados
    echo "<table border='1'><tr><td>ID</td><td>NOMBRE</td></tr> ";
    foreach ($results as $row) {
        echo "<tr><td> " . $row['id'] . "</td> <td> " . $row['name'] . "</td></tr>";
    }
    echo "</table>";
} catch (PDOException $e) {
    die("Error al ejecutar la consulta: " . $e->getMessage());
}

// Cerrar la conexión
$pdo = null;

?>
