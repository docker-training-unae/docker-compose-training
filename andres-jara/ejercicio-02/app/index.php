<?php
    // Parámetros de conexión a la base de datos
    $host = 'db_service';         // Cambiar a la dirección de tu servidor de base de datos
    $port = '5432';       // Cambiar al puerto de tu servidor de base de datos
    $dbname = 'dockerdb';  // Cambiar al nombre de tu base de datos
    $user = 'dockeruser';      // Cambiar a tu nombre de usuario de PostgreSQL
    $password = 'v4!u3';// Cambiar a tu contraseña de PostgreSQL

    // Intentar establecer la conexión
    $conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

    // Verificar si la conexión fue exitosa
    if (!$conn) {
        die("Error de conexión: " . pg_last_error());
    } else {
        echo "</br>";
        echo "<h1>Conexión exitosa!</h1>";
    }

    // Realizar una consulta simple
    $query = "SELECT * FROM persona"; // Cambiar a tu consulta específica
    $result = pg_query($conn, $query);

    // Verificar si la consulta fue exitosa
    if (!$result) {
        die("Error en la consulta: " . pg_last_error());
    }

    echo "<h2>Registros encontrados</h2>";
    // Mostrar los resultados
    while ($row = pg_fetch_assoc($result)) {
        echo "ID: " . $row['id'] . " - Nombres: " . $row['nombres'] . " - Apellidos: " . $row['apellidos'] ."<br>";
    }

    // Cerrar la conexión
    pg_close($conn);
?>