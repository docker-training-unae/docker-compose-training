<?php
$host = 'postgres-db'; // Puede ser 'localhost' si PostgreSQL está en la misma máquina.
$port = '5434';
$dbname = 'mydatabase';
$user = 'myuser';
$password = 'mypassword';

try {
    // Conectar a la base de datos PostgreSQL usando PDO
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);

    // Realizar consultas, ejecutar comandos, etc.
    $query = $pdo->query("SELECT * FROM example");

// Crear tabla HTML
    echo "<table border='1'>";
    echo "<tr><th>ID</th><th>Nombre</th><th>Edad</th></tr>";
while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['name'] . "</td>";
        echo "<td>" . $row['edad'] . "</td>";
        echo "</tr>";
    }

    echo "</table>";

    // Cerrar la conexión
    $pdo = null;
} catch (PDOException $e) {
    // Manejar errores de conexión
    echo "Error de conexión: " . $e->getMessage();
}
?>
