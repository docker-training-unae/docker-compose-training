<?php

// Configuración de la conexión a la base de datos
$host = 'db';
$dbname = 'mydb';
$port = '5432';
$user = 'myuser';
$password = 'mypass';

// Intentar conectar a la base de datos
try {
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error al conectar a la base de datos: " . $e->getMessage());
}

// Ejemplo de consulta
try {
    // Preparar y ejecutar la consulta
    $query = "SELECT * FROM usuario";
    $statement = $pdo->prepare($query);
    $statement->execute();

    // Obtener resultados
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    // Imprimir resultados
    echo "<table border='1'><tr><td>ID</td><td>NOMBRE</td></tr> ";
    foreach ($results as $row) {
        echo "<tr><td> " . $row['id'] . "</td> <td> " . $row['nombre'] . "</td></tr>";
    }
    echo "</table>";
} catch (PDOException $e) {
    die("Error al ejecutar la consulta: " . $e->getMessage());
}

// Cerrar la conexión
$pdo = null;

?>

