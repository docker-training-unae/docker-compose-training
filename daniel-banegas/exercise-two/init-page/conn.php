<?php

function getDB() {
  $host = "db";
  $dbname = "postgres";
  $user = "myuser";
  $pass = "mypass";
  
  try {
    $dsn = "pgsql:host=$host;dbname=$dbname";
    $conn = new PDO($dsn, $user, $pass);
    return $conn;

  } catch (PDOException $e) {
    echo $e->getMessage();
  }
}

?>