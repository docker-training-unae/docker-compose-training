

<?php

require_once("conn.php");

$conn = getDB();

$sql = "SELECT * FROM public.example";
$result = $conn->query($sql);
?>
<table class="table table-bordered table striped">
    <?php while($row = $result->fetch()) { ?>
    <tr>
        <td><?php echo $row['name'] ?></td>
    </tr>
    <?php } ?>
</table>