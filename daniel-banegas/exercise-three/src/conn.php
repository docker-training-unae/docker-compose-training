<?php

function getDB() {
  $host = "db2";
  $dbname = "postgres";
  $user = "user123";
  $pass = "pass123";
  
  try {
    $dsn = "pgsql:host=$host;dbname=$dbname";
    $conn = new PDO($dsn, $user, $pass);
    return $conn;

  } catch (PDOException $e) {
    echo $e->getMessage();
  }
}

?>